import React, { useEffect, useState } from 'react';

const Filter = ({getCurrentFilter,brightness,grayscale,sepia,contrast,saturate,hueRotate}) => {

    const filterElement = [
        {
            name: 'brightness',
            value:brightness
        },
        {
            name: 'grayscale',
            value: grayscale
        },
        {
            name: 'sepia',
            value:sepia
        },
        {
            name: 'saturate',
            value: saturate
        },
        {
            name: 'contrast',
            value: contrast
        },
        {
            name: 'hueRotate',
            value: hueRotate
        }
    ]

    const [current,setCurrent] = useState("brightness");
    const [currentData,setCurrentData] = useState();

    useEffect(() => {
        filterElement.forEach((item) => {
            if (item.name === current) {
                console.log("filterd name ",item.name);
                setCurrentData(item.value);
                return;
            }
        })
    },[current])

    useEffect(() => {
        getCurrentFilter({current,currentData})
    },[currentData])

  return (
    <div>
        <div className='grid grid-cols-2 gap-6 px-3 py-4'>
            {filterElement.map((element,index) => (
                <button key={index} className={`border py-3 rounded-sm border-purple-600 font-bold ${element.name === current ? "bg-orange-600":""}`}
                    onClick={() => setCurrent(element.name)}
                >{element.name}</button>
            ))

            }

            <div className='flex flex-col py-6'>
                <div className='flex items-center justify-between w-[300px]'>
                    <label htmlFor="">{current}</label>
                    <p>100 %</p>
                </div>
                <input type="range" className='w-[300px]' value={currentData} max={200} onChange={(e) => setCurrentData(e.target.value)}/>
            </div>
        </div>
    </div>
  )
}

export default Filter