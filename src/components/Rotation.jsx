import React from 'react';
import {GrRotateLeft,GrRotateRight} from 'react-icons/gr';
import {CgMergeVertical,CgMergeHorizontal} from 'react-icons/cg';

const Rotation = ({leftRotation,rightRotation, verticalFlip, horizontalFlip}) => {
  return (
    <div className='flex justify-start items-center space-x-6 py-4'>
        <div className='border px-6 py-3'
            onClick={leftRotation}
        ><GrRotateLeft /></div>
        <div className='border px-6 py-3'
            onClick={rightRotation}
        ><GrRotateRight /></div>
        <div className='border px-6 py-3'
             onClick={horizontalFlip}
        ><CgMergeVertical /></div>
        <div className='border px-6 py-3'
            onClick={verticalFlip}
        ><CgMergeHorizontal /></div>
    </div>
  )
}

export default Rotation