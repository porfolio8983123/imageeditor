import { useEffect, useState } from 'react'
import { FaRegFolderOpen } from "react-icons/fa";
import image1 from '../src/assets/image.jpg';
import { FaSave } from "react-icons/fa";
import Filter from './components/Filter';
import Rotation from './components/Rotation';
import {IoMdUndo, IoMdRedo} from 'react-icons/io';
import 'react-image-crop/dist/ReactCrop.css'
import ReactCrop from 'react-image-crop';
import storeData from './components/LinkedList';


function App() {
  const [circular,setCircular] = useState(false);
  const [details,setDetails] = useState('');
  const [crop,setCrop] = useState('');
  const [image, setImage] = useState({
    image: '',
    brightness: 200,
    grayscale: 0,
    sepia: 0,
    saturate: 200,
    contrast: 100,
    hueRotate: 0,
    rotate: 0,
    vertical: 1,
    horizontal: 1
  })

  const handleFolderIconClick = (event) => {
    if (event.target.files.length !== 0) {
      const reader = new FileReader(); 
  
      reader.onload = () => {
        const imageData = reader.result;
  
        setImage({
          ...image,
          image: imageData
        });
  
        const stateData = {
          image: imageData,
          brightness: 200,
          grayscale: 0,
          sepia: 0,
          saturate: 200,
          contrast: 100,
          hueRotate: 0,
          rotate: 0,
          vertical: 1,
          horizontal: 1
        };
  
        storeData.insert(stateData);
        console.log("image hello", imageData);
      };
  
      reader.readAsDataURL(event.target.files[0]);
    }
  };

  console.log("image ",image);

  const getCurrentFilter = ({current,currentData}) => {
    setImage({
      ...image,
      [current]:currentData
    })
  }

  const leftRotation = () => {
    setImage({
      ...image,
      rotate: image.rotate - 90
    })

    const stateData = image;
    stateData.rotate = image.rotate-90;

    storeData.insert(stateData);
  }

  const rightRotation = () => {
    setImage({
      ...image,
      rotate: image.rotate + 90
    })

    const stateData = image;
    stateData.rotate = image.rotate + 90;

    storeData.insert(stateData);
  }

  const verticalFlip = () => {
    console.log("Before Vertical Flip:", image.vertical);
    setImage({
      ...image,
      vertical: image.vertical === 1 ? -1 : 1 // Toggle vertical flip state
    });
    console.log("After Vertical Flip:", image.vertical);

    const stateData = image;
    stateData.vertical = image.vertical === 1 ? -1 : 1;

    storeData.insert(stateData);
  }

  const horizontalFlip = () => {
    console.log("Before Vertical Flip:", image.vertical);
    setImage({
      ...image,
      horizontal: image.horizontal === 1 ? -1 : 1 // Toggle vertical flip state
    });
    console.log("After Vertical Flip:", image.vertical);

    const stateData = image;
    stateData.horizontal = image.horizontal === 1 ? -1 : 1;

    storeData.insert(stateData);
  }

  const redo = () => {
    const data = storeData.redoEdit();
    if (data) {
      setImage(data);
    }
  }

  const undo = () => {
    const data = storeData.undoEdit();
    if (data) {
      setImage(data);
    }
  }

  const saveImage = () => {
    const canvas = document.createElement('canvas');
    canvas.width = details.naturalWidth;
    canvas.height = details.naturalHeight;

    const ctx = canvas.getContext('2d');

    ctx.filter = `brightness(${image.brightness}%) sepia(${image.sepia}%) saturate(${image.saturate}%) contrast(${image.contrast}%) grayscale(${image.grayscale}%) hue-rotate(${image.hueRotate}deg)`;
    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.rotate(image.rotate * Math.PI / 180)
    ctx.scale(image.vertical, image.horizontal)

    ctx.drawImage(
      details,
      -canvas.width/2,
      -canvas.height / 2,
      canvas.width,
      canvas.height
    )

    const link = document.createElement('a');
    link.download = "image_edit.jpg";
    link.href = canvas.toDataURL();
    link.click();
  }

  const cropImage = () => {
    const canvas = document.createElement('canvas');
    console.log('natural width ',details.width);
    const scaleX = details.naturalWidth / details.width;
    const scaleY = details.naturalHeight / details.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      details,
      crop.x * scaleX,
      crop.y*scaleY,
      crop.width*scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    )

    const base64Url = canvas.toDataURL('/image/jpg');

    setImage({
      ...image,
      image:base64Url
    })
  }

  useEffect(() => {
    console.log("details ",details);
  },[details])

  return (
    <div className='flex justify-center h-screen items-center'>
      <div className='px-20'>
        <div class="bg-white border h-[600px] shadow-xl rounded-lg">
          <div className='flex items-center'>
            <div class="flex p-2 gap-1 items-center">
              <div class="">
                <span class="bg-blue-500 inline-block center w-3 h-3 rounded-full"></span>
              </div>
              <div class="circle">
                <span class="bg-purple-500 inline-block center w-3 h-3 rounded-full"></span>
              </div>
              <div class="circle">
                <span class="bg-pink-500 box inline-block center w-3 h-3 rounded-full"></span>
              </div>
            </div>
            <div className='w-full h-[50px] flex items-center'>
              <div class="ml-6 flex items-center justify-between w-full pr-10">
                <div>
                  <label htmlFor="fileUpload"><FaRegFolderOpen size={32}/></label>
                  <input id = "fileUpload" className='fileUpload' type="file"  onChange={handleFolderIconClick} style={{display:'none'}}/>
                </div>

                <div>
                  <FaSave onClick={saveImage} className='cursor-pointer' size={25}/>
                </div>
               </div>
            </div>
          </div>
          <div class="px-6 flex justify-center items-center">
            <div className='h-full'>
              <Filter getCurrentFilter = {getCurrentFilter} brightness = {image.brightness} grayscale = {image.grayscale} sepia = {image.sepia} saturate = {image.saturate} contrast = {image.contrast} hueRotate = {image.hueRotate}/>
              <div className='px-3'>
                <h4 className='font-bold'>Rotate & Flip</h4>
                <Rotation leftRotation = {leftRotation} rightRotation = {rightRotation} verticalFlip = {verticalFlip} horizontalFlip = {horizontalFlip}/>
              </div>

              <div className='py-4 flex items-center space-x-4 px-3'>
                <button className='border px-6 py-3' onClick={undo}><IoMdUndo /></button>
                <button className='border px-6 py-3' onClick={redo}><IoMdRedo /></button>
                {crop &&
                  <>
                    <button className='border px-6 py-2 bg-orange-600 rounded-xl hover:scale-110 transition-all duration-300 hover:bg-gradient-to-r from-orange-600 to-black hover:text-white'
                      onClick={cropImage}
                    >Crop</button>
                  </>
                }
              </div>

            </div>
            <div className='flex justify-center border' style={{width:'100%',height:'500px'}}>
              <div className='border border-red-800 flex justify-center items-center' style={{height:'100%',width:'100%', overflowY:'scroll',overflowX:'scroll'}}>
                {image.image ? 
                  <ReactCrop crop={crop} onChange={c => setCrop(c)} style={{ maxHeight: '100%', maxWidth: '100%' }}>
                    <img onLoad={(e) => setDetails(e.currentTarget)} src={image.image} alt="" className='uploadingImage transition-all duration-300' 
                      style={{filter: `brightness(${image.brightness}%) sepia(${image.sepia}%) saturate(${image.saturate}%) contrast(${image.contrast}%) grayscale(${image.grayscale}%) hue-rotate(${image.hueRotate}deg)`,transform: `rotate(${image.rotate}deg) scaleX(${image.horizontal}) scaleY(${image.vertical})`}}
                    />
                  </ReactCrop>
                  :<img src={image1} alt="" className='uploadingImage' />
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default App
